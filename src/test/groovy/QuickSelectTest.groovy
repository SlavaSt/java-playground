import spock.lang.Specification
import spock.lang.Unroll

import java.util.concurrent.ThreadLocalRandom
import java.util.stream.IntStream

import static QuickSelect.partition
import static java.util.stream.Collectors.toList


class QuickSelectTest extends Specification {

    @Unroll("#list")
    def "should select min n elements from Collection"() {
        expect:
        QuickSelect.takeMinElements(list, minN).sort() == expected

        where:
        list                                                       | minN | expected
        [1]                                                        | 1    | [1]
        [2, 1]                                                     | 1    | [1]
        shuffle(IntStream.range(1, 100).boxed().collect(toList())) | 5    | [1, 2, 3, 4, 5]
        [8, 5, 2, 6, 1, 3, 9, 7, 4]                                | 5    | [1, 2, 3, 4, 5]

    }

    @Unroll("#list")
    def "should select min n elements"() {
        given:
        def array = list as Integer[]
        QuickSelect.select(array, 0, array.size() - 1, minN)

        expect:
        Arrays.stream(array).limit(minN).collect(toList()).sort() == expected

        where:
        list                                       | minN | expected
        [1]                                        | 1    | [1]
        [2, 1]                                     | 1    | [1]
        shuffle(IntStream.range(1, 100).toArray()) | 5    | [1, 2, 3, 4, 5]
        [8, 5, 2, 6, 1, 3, 9, 7, 4]                | 5    | [1, 2, 3, 4, 5]

    }

    @Unroll("list expected: #list #expected")
    def "should partition around pivot"() {
        given:
        def array = list as Integer[]
        def pivotIndex = partition(array, 0, array.length - 1, pivotIdx)

        expect:
        array == expected
        pivotIndex == expectedPivotIndex

        where:
        list                           | pivotIdx | expected                       | expectedPivotIndex
        [1]                            | 0        | [1]                            | 0
        [1, 2]                         | 0        | [1, 2]                         | 0
        [1, 2]                         | 1        | [1, 2]                         | 1
        [2, 1]                         | 1        | [1, 2]                         | 0
        [2, 1]                         | 1        | [1, 2]                         | 0
        [5, 3, 8, 1, 2, 4, 6, 9, 8, 7] | 0        | [3, 1, 2, 4, 5, 7, 6, 9, 8, 8] | 4
        [5, 3, 8, 1, 2, 4, 6, 9, 8, 7] | 2        | [5, 3, 7, 1, 2, 4, 6, 8, 8, 9] | 7
    }

    private <T> List<T> shuffle(List<T> list) {
        Collections.shuffle(list);
        list
    }

    private static int[] shuffle(int[] ar) {
        // If running on Java 6 or older, use `new Random()` on RHS here
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--) {
            int index = rnd.nextInt(i + 1);
            // Simple swap
            int a = ar[index];
            ar[index] = ar[i];
            ar[i] = a;
        }
        ar
    }

}
