import java.util.*;

import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.Collections.shuffle;
import static java.util.stream.Collectors.toList;

/**
 * Created by eugene on 06/12/15.
 */
public final class QuickSelect {

    private QuickSelect() {
    }


    public static <T extends Comparable<T>> List<T> takeMinElements(Collection<T> col, int n) {
        if (n == 0) {
            return new ArrayList<>();
        }
        if (n > col.size()) {
            return new ArrayList<>(col);
        }
        T[] arr = asArray(col);
        select(arr, 0, col.size() - 1, n);
        return stream(arr).limit(n).collect(toList());
    }

    static <T extends Comparable<T>> T select(T[] arr, int left, int right, int n) {
        shuffle(asList(arr));
        while (left != right) {
            int pivotIdx = partition(arr, left, right, left);
            if (pivotIdx < n) {
                left = pivotIdx + 1;
            } else if (pivotIdx > n) {
                right = pivotIdx - 1;
            } else {
                break;
            }
        }
        return arr[n - 1];
    }

    /**
     * < | >=
     *
     * @param arr
     * @param pivotIdx
     * @return
     */
    static <T extends Comparable<T>> int partition(T[] arr, int left, int right, int pivotIdx) {
        if (arr.length == 0) {
            throw new IllegalArgumentException("cannot partition empty list");
        }
        T pivotValue = arr[pivotIdx];
        swap(arr, pivotIdx, right); // move pivot to the end to get it out of the way
        int storeIdx = left;
        for (int i = left; i < arr.length; i++) {
            if (arr[i].compareTo(pivotValue) < 0) {
                swap(arr, storeIdx++, i);
            }
        }
        swap(arr, storeIdx, right); // restore pivot to its rightful position
        return storeIdx;
    }

    private static void swap(Object[] arr, int i, int j) {
        Object temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static <T extends Comparable<T>> T[] asArray(Collection<T> col) {
        //noinspection unchecked
        return col.toArray((T[]) new Comparable[col.size()]);
    }

}
